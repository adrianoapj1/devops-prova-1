# DevOps Example

Serviço que retorna a hora atual do servidor em uma timezone específica.

### 1. Inicialização da aplicação

```
make up
```

### 2. Instalar dependências


```
make php
make install
```

### 3. Executar a aplicação em ambiente local

```
make php
make start
```

### 4. Executar testes em ambiente local

```
make php
make test
```

### 5. Utilizar a aplicação em ambiente local

No navegador digite:

```
http://localhost:8081?timezone=America/Sao_paulo
```

Para outras timezones acesse: https://www.php.net/manual/en/timezones.php

Olá, se você está lendo isso, saiba que é apenas um teste :)